package com.yarenty.strategy;

/**
 * Created by yarenty on 16/02/2015.
 */
public interface Strategy {

    public int doOperation(int num1, int num2);
}
