package com.yarenty.adapter;

/**
 * Created by yarenty on 02/02/15.
 */
public interface AdvancedMediaPlayer {
    public void playVlc(String fileName);
    public void playMp4(String fileName);
}