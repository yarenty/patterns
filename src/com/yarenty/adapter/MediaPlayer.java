package com.yarenty.adapter;

/**
 * Created by yarenty on 02/02/15.
 */
public interface MediaPlayer {
    public void play(String audioType, String fileName);
}
