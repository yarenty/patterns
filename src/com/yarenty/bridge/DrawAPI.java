package com.yarenty.bridge;

/**
 * Created by yarenty on 02/02/15.
 */
public interface DrawAPI {
    public void drawCircle(int radius, int x, int y);
}
