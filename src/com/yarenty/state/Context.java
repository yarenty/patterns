package com.yarenty.state;

/**
 * Created by yarenty on 16/02/2015.
 */
public class Context {
    private State state;

    public Context() {
        state = null;
    }

    public void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }
}
