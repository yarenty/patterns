package com.yarenty.composite_entity;

/**
 * Created by yarenty on 18/02/2015.
 */
public class DependentObject2 {

    private String data;

    public void setData(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }
}
